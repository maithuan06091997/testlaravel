<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">
</head>
<body>
    <table class="table table-striped mb-0 ">
            <tr class="bg-fbfcfd color-AFAFAF">
                <th  class="text-center"><input type="checkbox" class="js-cb-toggle-all" data-targets="target_cbs_id"></th>
                <th  class="text-center" >{{ __('THUMBNAIL') }}</th>
                <th  class=""><a href="{{ $myOrder['title'] ?? '' }}">{{ __('TITLE') }}<i class="{{ $mySortIcon['iconTitle'] ?? '' }}"></i></a></th>
                <th  class=""><a href="{{ $myOrder['status'] ?? '' }}">{{ __('DESCRIPTION ') }}<i class="{{ $mySortIcon['iconStatus'] ?? '' }}"></i></a></th>
                <th class="text-center"><a href="{{ $myOrder['slug'] ?? '' }}">{{ __('CREATED_BY ') }}<i class="{{ $mySortIcon['iconSlug'] ?? '' }}"></i></a></th>
                <th  class="text-center">{{ __('OPERATIONS') }}</th>
            </tr>
        @if ($posts->count() > 0)
            @foreach ($posts as $post)
                <tr class="child">
                    <td class="text-center vertical-align-middle"><input type="checkbox" name="bulk_id[]" id="target_cbs_id" value="{{ $post->id }}"></td>
                    <td class="text-center vertical-align-middle"><img src="{{ asset(Storage::url($post->thumbnail)) }}" alt="#" width="50px"></td>
                    <td class=" vertical-align-middle"><a href="#" title="{{ $post->title }}">{{ $post->title }}</a></td>
                    <td class=" vertical-align-middle"><a href="#" title="{{ $post->title }}">{{ $post->description }}</a></td>
                    <td class="text-center vertical-align-middle">{{ $post->user->email }}</td>
                    <td class="text-center vertical-align-middle"> 
                        <a href="{{ route('admin.post.edit',$post->id) }}" class="btn btn-primary">{{ __('Edit') }}</a>
                        <label for="delete-{{ $post->id }}" class="btn btn-warning">{{ __('Delete') }}</label>
                    </td>
                </tr>
            @endforeach
        </table>
        @else
            </table> 
            <h4 class="text-danger text-center mt-15"> No Post</h4>
        @endif
</body>
</html>