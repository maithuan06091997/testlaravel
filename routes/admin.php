<?php

Route::group(['namespace' => 'Post', 'prefix' => 'post', 'as' => 'post.'], function () {
    Route::get('/', 'IndexController')->name('index');
    Route::get('create', 'CreateController')->name('create');
    Route::post('store', 'StoreController')->name('store');
    Route::get('edit/{id}', 'EditController')->name('edit');
    Route::put('update/{id}', 'UpdateController')->name('update');
    Route::delete('delete/{id}', 'DeleteController')->name('delete');
    Route::delete('bulk', 'BulkController')->name('bulk');
    Route::get('export','ExportController')->name('export');
    Route::get('email','EmailController')->name('email');
    Route::post('send', 'SendEmailController')->name('send');

});
