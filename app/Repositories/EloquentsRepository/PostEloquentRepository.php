<?php

namespace App\Repositories\EloquentsRepository;

use App\Post;
use App\Events\PostCloneTag;
use App\Events\PostCloneCategory;
use App\Repositories\EloquentRepository;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;

class PostEloquentRepository extends EloquentRepository implements IPostDbRepository{
    public function __construct(){
        $this->setModel();
    }
    public function getModel(){
        return Post::class;
    }


}