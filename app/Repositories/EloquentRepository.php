<?php

namespace App\Repositories;

use App\Contracts\IDbRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

abstract class EloquentRepository implements IDbRepository{
    protected $model;

    abstract public function getModel();

    public function setModel(){
        $this->model = app()->make($this->getModel());
    }
    public function getAll(){
        return $this->model->all();
    }

    public function find(int $id){
        return $this->model->find($id);
    }


    public function create(array $data){
        $this->model->fill($data);
        $this->model->save();
        return $this->model->id;
    }
    public function update(int $id,array $data){
        $this->model->where('id', $id)->update($data);
    }

    public function delete(int $id){
        $this->model->where('id', $id)->delete();
    }

 
    public function bulkDelete(array $id){
        $this->model->whereIn('id', $id)->delete();
    }

    public function bulk(string $action,array $id){
        return $this->$action($id);
    }

    public function paginate(int $limit = 5,array $creatials=[]){
        return $this->model->where($creatials)->paginate($limit);
    }
}