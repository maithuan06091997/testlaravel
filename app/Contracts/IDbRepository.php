<?php

namespace App\Contracts;


interface IDbRepository{
    public function getAll();
    public function find(int $id);
    public function create(array $data);
    public function update(int $id,array $data);
    public function delete(int $id);
    public function bulkDelete(array $id);
}