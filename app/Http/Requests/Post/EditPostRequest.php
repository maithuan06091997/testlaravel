<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class EditPostRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:250|unique:posts,title,'.$this->id,
            'thumbnail' => 'image|min:1|max:500|mimes:jpeg,png,svg',
            'description' => 'required|max:250',
            'content' => 'required|max:5000'
        ];
    }
}