<?php

namespace App\Http\Controllers\Post;

use App\Post;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    public function __invoke(){
        return view('posts.email');
    }
}
