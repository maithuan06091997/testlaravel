<?php
namespace App\Http\Controllers\Post;
 
use PDF;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;
  
class SendEmailController extends Controller
{
    protected $postRepository;

    public function __construct(IPostDbRepository $postRepository){
        $this->postRepository = $postRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $input = $request->all();
        Mail::send('mailfb', array('file'=>$input["file"]->getClientOriginalName(),'email'=>$input["email"], 'content'=>$input['comment'], 'name'=>$input["name"]), function($message){
	        $message->to('plachym.it@gmail.com', 'Visitor')->subject('Visitor Feedback!');
	    });
        Session::flash('flash_message', 'Send message successfully!');

        return view('posts.email')->with(['Create'=>'Create Successfully','Alert'=>'Create']);
    }
}