<?php
namespace App\Http\Controllers\Post;
 
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;
  
class ExportController extends Controller
{
    protected $postRepository;

    public function __construct(IPostDbRepository $postRepository){
        $this->postRepository = $postRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $posts = $this->postRepository->getAll();
        //  $dataView = compact($posts);
        $data = ['posts' => $posts];
        $pdf = PDF::loadView('posts.myPDF', $data);
        return $pdf->download('hoatam.pdf');
    }
}