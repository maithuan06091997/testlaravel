<?php

namespace App\Http\Controllers\Post;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class CreateController extends Controller
{
   

    public function __construct(){
     
    }

    public function __invoke(){
        
        return view('posts.create');
    }
}
