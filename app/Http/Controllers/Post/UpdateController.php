<?php

namespace App\Http\Controllers\Post;

use Auth;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Post\EditPostRequest;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;


class UpdateController extends Controller
{
    protected $postRepository;

    protected $fillData = ['title','thumbnail','description','content','slug','status'];

    public function __construct(IPostDbRepository $postRepository){
        $this->postRepository = $postRepository;
    }

    public function __invoke($id, EditPostRequest $request){
        $post = $this->postRepository->find($id);
        $dataRequest = $request->only($this->fillData);
        $dataRequest['user_id'] = Auth::user()->id;
        $file = $request['thumbnail'];
        if(!empty($file) ){
            $filename =  Str::uuid().$file->getClientOriginalName();
            $dataRequest['thumbnail'] = $file->storeAs('img', $filename, 'public');
            // delete Thumbnail Old
            $postOldImg = public_path('storage/'.$post->thumbnail);
            if(file_exists($postOldImg)){
                unlink($postOldImg);
            }
        }
        
        $this->postRepository->update($id, $dataRequest);
        return redirect()->route('admin.post.index')->with(['Update'=>'Update Successfully','Alert'=>'Update']);
    }
}
