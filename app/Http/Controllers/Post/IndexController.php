<?php

namespace App\Http\Controllers\Post;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;

class IndexController extends Controller
{
    protected $postRepository;

    public function __construct(IPostDbRepository $postRepository){
        $this->postRepository = $postRepository;
    }
    public function __invoke(Request $request){
        $posts =  $this->postRepository->paginate(5);
        $dataView = compact('posts');
        return view('posts.list',$dataView);
    }
}


