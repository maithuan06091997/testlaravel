<?php

namespace App\Http\Controllers\Post;

use Auth;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Post\InsertPostRequest;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;


class StoreController extends Controller
{
    protected $postRepository;

    public function __construct(IPostDbRepository $postRepository){
        $this->postRepository = $postRepository;
    }
    
    public function __invoke(InsertPostRequest $request){
        $dataRequest = $request->all();
        $dataRequest['user_id'] = Auth::user()->id;
        // custom thumbnail
        $file = $request['thumbnail'];
        $filename =  Str::uuid().$file->getClientOriginalName();
        $dataRequest['thumbnail'] = $file->storeAs('img', $filename, 'public');
        // create post
        $idPost = $this->postRepository->create($dataRequest);
        // sync Category & Tag
        return redirect()->route('admin.post.index')->with(['Create'=>'Create Successfully','Alert'=>'Create']);
    }
}
