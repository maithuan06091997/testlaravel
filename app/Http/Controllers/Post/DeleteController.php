<?php

namespace App\Http\Controllers\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;

class DeleteController extends Controller
{
    protected $postRepository;

    public function __construct(IPostDbRepository $postRepository){
        $this->postRepository = $postRepository;
    }

    public function __invoke(int $id){
        $post = $this->postRepository->find($id);
        $postOldImg = public_path('storage/'.$post->thumbnail);
        if(file_exists($postOldImg)){
            unlink($postOldImg);
        }
        $this->postRepository->delete($id);
        return redirect()->back()->with(['Delete'=>'Delete Successfully','Alert'=>'Delete']);
    }
}
