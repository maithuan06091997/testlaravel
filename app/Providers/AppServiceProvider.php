<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\EloquentsDbRepository\IPostDbRepository;
use App\Repositories\EloquentsRepository\PostEloquentRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IPostDbRepository::class, PostEloquentRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
